<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test case MyID</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>1f4e497d-b66f-44ba-9641-3040088ba3dc</testSuiteGuid>
   <testCaseLink>
      <guid>9b67d201-1e36-42bc-a784-58a5f087d30a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Login Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e7cdc78f-76f0-459f-86f3-3584e4e8c189</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Login Fail (Password wrong)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c781a28f-b080-4a34-b8e6-eb6127aeac0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Change Password Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c8d0862-1612-4540-8ffe-11169ca6431c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Change Password Fail (Length)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6630cb2a-dc3c-4890-b2e8-26d0bc5ef2d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Change Password Fail (New Password not match)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>74015d89-67ab-47b8-bdf5-7dcf1a5f251a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Change Password Fail (No character)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>256a207d-c216-44f2-a5f2-82523a88e75a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Change Password Fail (No number)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>497cc82d-2d79-4c75-b711-e02555e7c264</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Change Password Fail (Same password old and new)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>079e6415-40b4-4dec-b426-b05cb0de36d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Change Password Fail (No symbol)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ba785f09-7711-432d-97e8-8cb6f102f7e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
